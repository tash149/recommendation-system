## Team 7 - Improved Product Recommendation System

### Project Overview

A brief description of 
* Problem Statement: Based on the user's history across the internet, we need to develop an improved product recommendation system which can give better surface area for Sales team as well as improve online product recommendation for cusstomers.

* Solution: Web Interface integrated with product recommendation system which provides recommendation based on user's history. The user's history is tracked through a web extension, via Chrome APIs. 

### Solution Description

Our solution covers all the bases mentioned in the problem statement.
	
1) User History: Our team has successfully built and web extension, which can be used in any web browsers, eg. Chrome, Opera, Edge,etc. Dell respects and follows data privacy and for the same reason we have aligned our model according to the data privacy rules of Dell. The extension fetches the relevant data from the user's browsing history. The browsing history related only to the electronics will be tracked, because any other data is irrelevant for our purpose. This history is then sent via Web Services API to the database. 

2) Recommendation Engine: The recommendation engine now fetches the user history, which is in partial filtered state because of the extension which provided us only with meaningful electonics data. Now, through Natural Language Processing we do semantic analysis, on the user's history and we identify meaningful tags that are either related to the Laptops or the accessories. We use decision tree classfier and hamming distance to match where the user preference lies and hence traverse to the leaves of the decision tree, where will be get the highest matched tags. These tags will be matched against the laptop's tags in the database and will be displayed on the website under "Recommended Products".

3) Reason Box: For this particular use case, we have implemented a chatbot which will analyse the the reviews given by the users and this data can be used for further training the AI Engine, Increased and enhanced Customer Experience.

4) Backend Analytics: Backend Analytics is provided through data visualisation on Tableau. The backend analytics is integrated with our web portal but can be viewed only by the admin team. It provided really intuitive charts and statistics based on how the recommendation system has helped increase the order conversion rate and the overall revenue generation at dell.com

#### Architecture Diagram

![](Architectural_Diagram.png)

#### Technical Description

Tech Stack Used: 
* Angular 6
* Django 2.1
* Tensorflow 2.0
* Javascript


### Team Members
----------------------------------

1. Pratik Jain: pratikjain0411@gmail.com

   Contributions: Web Interface using Angular, Natural Language Processing for tag filtering, REST APIs Django

2. Ateek Ujjawal: ateekujjawal@gmail.com

   Contributions: Built the web interface data fetching algorithms in Angular, Added keyword picking algo

3. Tarushi Sharma: tarushisharmaace@gmail.com
  
   Contributions: Backend Analytics, and Decision Tree Classication, Hamming distance algorithm, NLP for tagging

4. Kalpaj Agrawalla: kalpaj12@gmail.com

   Contributions: Extension for fetching user history using Chrome APIs, Data Filtering Algorithm


