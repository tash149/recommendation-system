from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from api import views


urlpatterns = [
    path('history/', views.HistoryList.as_view(), name='history'),
    path('history/last/', views.HistoryList.getLastHistory),
    path('product/', views.ProductView.as_view(), name='product'),
    path('product/<int:pk>/', views.ProductByIDView.getProd),
    path('product/update/', views.ProductUpdateView.as_view()),
    path('product/tags/', views.TagsView.as_view()),
    path('results/', views.ResultView.as_view(), name='results'),
    path('products/', views.TagsView.as_view(), name='products'),
    path('rec_product/', views.RecProductView.as_view()),
    path('rand_products/', views.RandProductView.as_view())
]

urlpatterns = format_suffix_patterns(urlpatterns)
