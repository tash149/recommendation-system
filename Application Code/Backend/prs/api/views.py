from django import http
from rest_framework import generics
from . models import History, Product, Result
from . serializers import HistorySerializer, ProductSerializer, ResultSerializer
from .model import neural_net
import random

class HistoryList(generics.ListCreateAPIView):
    queryset = History.objects.all()
    serializer_class = HistorySerializer

    def getLastHistory(self):
        history = History.objects.all().order_by('-id').values()[:10]
        new_history = list(history)
        pass_history = []
        pass_prices = []
        pass_names = []
        for item in history:
            pass_history.append(item['search_history'])
            pass_prices.append(item['price'])
            pass_names.append(item['product_name'])
        result = neural_net.getTags(pass_history, pass_prices, pass_names)
        return http.JsonResponse(result, safe=False)


class RecProductView(generics.ListCreateAPIView):
    def post(self, request, *args, **kwargs):
        price = int(0.25 * request.data['price'])
        price_l = request.data['price'] - price
        price_h = request.data['price'] + price
        tag = request.data['tag']
        products = list(Product.objects.filter(tags=tag, price__gte=price_l).filter(price__lte=price_h).values())
        return http.JsonResponse(products, safe=False)


class RandProductView(generics.ListCreateAPIView):
    def get(self, request, *args, **kwargs):
        products = list(Product.objects.filter(price__gte=50000).values())
        products1 = list(Product.objects.filter(price__lte=50000).values())
        new_products = []
        new_products.append(products)
        new_products.append(products1)
        return http.JsonResponse(new_products, safe=False)


class ProductView(generics.ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ProductUpdateView(generics.ListCreateAPIView):
    def post(self, request, *args, **kwargs):
        Product.objects.filter(id=request.data['id']).update(image=request.data['image'])
        return http.JsonResponse('{"status":"succesful"}', safe=False)


class ProductByIDView(generics.ListCreateAPIView):
    def getProd(self, pk):
        product = list(Product.objects.filter(id=pk).values())
        return http.JsonResponse(product, safe=False)


class ResultView(generics.ListCreateAPIView):
    queryset = Result.objects.all()
    serializer_class = ResultSerializer


class TagsView(generics.ListCreateAPIView):
    serializer_class = ProductSerializer

    def post(self, request, *args, **kwargs):
        tags = request.data['tags'].split(',')
        data = []
        products = Product.objects.all()
        for product in products:
            for tag in tags:
                if product.id not in data and tag in product.tags:
                    data.append(product.id)
        return http.JsonResponse(data, safe=False)