from rest_framework import  serializers
from . models import History, Product, Result


class HistorySerializer(serializers.ModelSerializer):

    class Meta:
        model = History
        fields = ('email', 'search_history', 'price', 'product_name')


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id', 'name', 'type', 'screen_inches',
                  'screen_resolution', 'cpu', 'ram', 'storage',
                  'gpu', 'os', 'weight', 'price', 'tags', 'image')


class ResultSerializer(serializers.ModelSerializer):

    class Meta:
        model = Result
        fields = ('product_id',)
