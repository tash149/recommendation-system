## first neural network with keras tutorial
# from numpy import loadtxt
import pandas as pd
#import random
#import tensorflow as tf
#from sklearn.utils import shuffle
from urllib.parse import urlparse,parse_qs
#from Datagen import Dataset

#tags=['goodbuildquality','powerful','cheap','accessories', 'lightweight', 'dailyuse', 'professional', 'yoga','performance']

#def nn_model(queries,targets):
#    X = tf.Tensor(queries, shape = (189,86), dtype = tf.float32)
#    y = tf.Tensor(targets, shape = (189,9), dtype = tf.float32)
#    model = tf.keras.Sequential([
#        tf.keras.layers.Dense(180, activation='relu', input_shape = 86),
#        tf.keras.layers.Dense(40, activation='relu'),
#        tf.keras.layers.Dense(50, activation='relu'),
#        tf.keras.layers.Dense(50, activation='relu'),
#        tf.keras.layers.Dense(9, activation='softmax')
#    ])
#    model.compile(loss='categorical_crossentropy', optimizer='RMSprop', metrics=['accuracy'])
#    model.fit(X,y, epochs = 15)
#

def queryToText(df):
    queries=[]
    for i in range(len(df)):
        flag=0
        parameters=''
        url=df.iloc[i][0]
        parsed = urlparse(url)
        parameters=parse_qs(parsed.query)
        if 'q' in parameters.keys():
            queries.append(parameters['q'][0])
        elif 'k' in parameters.keys():
            queries.append(parameters['k'][0])
        elif 'text' in parameters.keys():
            queries.append(parameters['text'][0])
        elif "currys" in url:
            url=url[8:].split("/")
            queries.append(url[4].replace("+", " "))

    return queries

def getwords(queries):
    words=[]
    for q in queries:
        for word in q:
            if not word in words:
                words.append(word)
    return words

        #1 for gaming

def getTags(histories, prices, names):
    dataframe = pd.DataFrame(histories)
    queries = queryToText(dataframe)
    if 'Default Name' in names:
        names.remove('Default Name')
    if '' in names:
        names.remove('')
    queries += names
    df=pd.read_csv('./gaming.csv')
    tags=[0.0 for i in queries]
    #prices=[]
    print(queries)
    for i,q in enumerate(queries):
            for keyWord in list(df['msi']):
                if keyWord in q.lower().split(" "):
                    tags[i]=1
            for word in q.lower().split(" "):
                if word:
                    if ord(word[-1]) in range(48,59) and ord(word[0]) in range(48,59) :
                        if float(word) >= 30000.0:
                            prices.append(float(word))

    p=[]
    for i in prices:
        if i >= 30000:
            p.append(i)
    avgPrice=sum(p)/len(p) if len(p) != 0 else 0.0
    tag=0 if len(tags) != 0  and (sum(tags)/len(tags)) < 0.5 else 1
    return [tag,avgPrice]

#words=getwords(queries)
#print(getTags([], [], []))
print(getTags([
    'https://www.amazon.in/FX505DY-BQ002T-15-6-inch-5-3550H-Windows-Graphics/dp/B07MSK34LL/ref=sr_1_3?keywords=gaming&qid=1570976812&smid=A14CZOWI0VEHLG&sr=8-3',
    'https://www.amazon.in/s?k=laptops+under+100000&ref=nb_sb_noss_1',
    'https://www.amazon.in/Dell-Inspiron-3480-i5-8265U-Graphics/dp/B07X4R6FVN/ref=sr_1_3?keywords=laptops+under+50000&qid=1570976629&smid=A14CZOWI0VEHLG&sr=8-3',
    'https://www.amazon.in/s?k=gaming+laptops&ref=nb_sb_noss'
],
    [68990.0, 0.0, 50225.0, 0.0],
    ['', '', '', '', ''
     ]))