from django.db import models


class History(models.Model):
    email = models.EmailField(null=False, default='')
    search_history = models.TextField()
    price = models.FloatField(default=0.0, null=False)
    product_name = models.TextField(default='')

    def __str__(self):
        return self.email

# todo: To make an API which provides product ids on sending tags as body


class Product(models.Model):
    id = models.IntegerField(primary_key=True, null=False)  # required
    name = models.CharField(max_length=1000, default='Dell Laptop', null=False)  # required
    price = models.FloatField(default=0.0, null=False)  # required
    image = models.CharField(max_length=1000, default='')
    type = models.TextField(default='')
    screen_inches = models.FloatField(default=0.0)
    screen_resolution = models.TextField(default='')
    cpu = models.TextField(default='')
    ram = models.TextField(default='')
    storage = models.TextField(default='')
    os = models.TextField(default='')
    gpu = models.TextField(default='')
    weight = models.TextField(default='')
    tags = models.TextField(default='')

    def __str__(self):
        return self.name


class Result(models.Model):
    product_id = models.TextField()  # list of product ids received from ML Model



