'use strict';

const DB = {

    // Debugger below (true for post to DB)
    PostDBisActive: true,

    // Endpoints
    HistoryEndpoint: {
        url: "https://dellhistory.pythonanywhere.com/history/",
        searchQueryHistoryEndpoint: [
            "gaminglaptop", "Gaming", "Gaming laptops", "Laptop", "Laptops", "laptop",
            "dell", "laptops", "professional", "office", "desktops", "Desktops", "Desktop", "i3", "i5", "i7",
            "i9", "IdeaPad", "Lenovo", "Alienware", "msi", "gaming", "pubg", "legion", "alienware", "rog",
            "omen", "gta5", "graphiccard", "predator", "nvidia", "tuf", "nitro", "gtx", "gtx1050ti", "gtx1050",
            "gtx1650", "fortnite", "16gbram", "12gbram", "nitro5", "ideapadl340", "6gbgraphics", "amd",
            "deeplearning", "scar", "rtx2080", "corsaironei140", "32gbram", "dellg715", "razorblade",
            "logitechg512", "razerkraken", "razerblackwidowlite", "razerblade15", "predatorhelios", "rogstrixg ",
            "predatortriton", "g3 ", "gfcore ", "nitro5ryzen", "gf63", "rogzephyrus", "inspiron3000 ", "rogcore",
            "omencore", "pavilioncore", "inspiron7000 ", "nitro5", "aspirevxcore", "nitro7", "gvseries", "rogcore",
            "gvcore", "xps15", "predator17", "raider", "stealthcore ", "gtx1660", "steam", "fifa", "farcry", "battelfield",
            "g7", "glcore", "gecore", "fortnite", "callofduty", "gt75", "gs65", "gtx2080", "gtx2070", "i9", "razerblade15",
            "gp75", "rtx2070", "gf63", "amdryzen7", "amdrygzen5", "amdradeonrx560x", "gtx1660ti", "1tbssd", "rtx2060", "gtx1060",
            "mx150", "64gbram", "gaminglaptop", "mx230", "ideapad330", "origin"
        ],
        ignoreQueryHistoryEndpoint: [
            "google.com", "pythonanywhere", "firebase", "youtube", "github", "hackerearth", "google.co.in", "pprq7.com", "stackoverflow"
        ],
    },

    // Other req's
    initmaxHistoryFetch: 1000,
};

var forumUrl = 'https://dell-product-recommendation.firebaseapp.com';

async function sendPOSTDB(sendData, Endpoint) {
    if (DB.PostDBisActive === false) {
        console.log(sendData);
        console.log(Endpoint);
    }

    if (DB.PostDBisActive === true) {
        await fetch(Endpoint, {
            method: "POST",
            mode: "cors",
            body: JSON.stringify(sendData),
            headers: {
                "Content-Type": "application/json; charset=UTF-8"
            }
        }).then(function(response) {
            if (DB.PostDBisActive === false) {
                console.log(response);
            }
        });
    }
}

function urlsanityCheck(OptimalQuery, IgnoreQuery, currentQuery) {
    return (OptimalQuery.some(substring => currentQuery.toUpperCase().includes(substring.toUpperCase())) === true) &&
        !(IgnoreQuery.some(substring => currentQuery.toUpperCase().includes(substring.toUpperCase())));
}

function cleanPrice(price) {
    return price.replace(/\&nbsp;/g, '').replace(/,/g, '').replace(/₹/, '');
}

function initialFetchHistory(timeconfig, userEmail) {
    chrome.runtime.sendMessage({
        text: "",
        action: "getHistory",
        startTime: timeconfig.oneWeekAgo,
        endTime: timeconfig.currentTime,
        initmaxHistoryFetch: DB.initmaxHistoryFetch,
    }, function(initialFetchHistoryresponse) {
        initialFetchHistoryresponse.history.forEach((history) => {
            if (urlsanityCheck(DB.HistoryEndpoint.searchQueryHistoryEndpoint,
                    DB.HistoryEndpoint.ignoreQueryHistoryEndpoint, history.url)) {
                var userHistoryData = {
                    email: userEmail,
                    search_history: history.url,
                    price: 0
                };
                sendPOSTDB(userHistoryData, DB.HistoryEndpoint.url);
            }
        });
    });
}

function initialUserConfig(timeconfig) {
    chrome.runtime.sendMessage({
        action: "getUserEmail"
    }, function(response) {
        // response has email address and unique id
        // console.log(response);
        var userEmail = response.user.email;

        chrome.storage.sync.set({
            'DELLextension': response
        }, function() {
            initialFetchHistory(timeconfig, userEmail);
            console.log(response);
            console.log("Saved initialization run, DB updating");
        });
    });

    chrome.storage.sync.set({
        searched_websites: 0
    }, function() {
        console.log('searched_websites is set to 0');
    });
}

function removeLastSync() {
    chrome.storage.sync.remove(['DELLextension']);
    chrome.storage.sync.remove(['searched_websites']);
}

// Extension main starts here
chrome.storage.sync.get(['DELLextension'], function(result) {
    if ((['DELLextension'] in result) === false) {
        var timeconfig = {
            currentTime: (new Date).getTime(),
            microsecondsPerWeek: 1000 * 60 * 60 * 24 * 7,
        };
        timeconfig.oneWeekAgo = timeconfig.currentTime - timeconfig.microsecondsPerWeek;
        initialUserConfig(timeconfig);
    } else {
        if (DB.PostDBisActive === false) {
            console.log("User exists in DB");
            console.log(result);
        }

        var currenturl = window.location.href;
        var userEmail = result.DELLextension.user.email;
        var price = 0;
        var product_name = "Default Name";

        if (urlsanityCheck(DB.HistoryEndpoint.searchQueryHistoryEndpoint,
                DB.HistoryEndpoint.ignoreQueryHistoryEndpoint, currenturl)) {
            if (currenturl.includes("flipkart")) {
                var getPrice = document.getElementsByClassName("_1vC4OE _3qQ9m1")[0];
                if (getPrice) {
                    price = cleanPrice(getPrice.textContent);
                }
                var getName = document.getElementsByClassName("_35KyD6")[0];
                if (getName) {
                    product_name = getName.textContent;
                }

            } else if (currenturl.includes("amazon")) {
                var getPrice = document.getElementById("priceblock_ourprice") ||
                    document.getElementById("priceBlockStrikePriceString a-text-strike") ||
                    document.getElementsByClassName("priceBlockStrikePriceString a-text-strike")[0] || "0";

                if (getPrice.innerHTML) {
                    price = cleanPrice(getPrice.innerHTML);
                } else if (getPrice.textContent) {
                    price = cleanPrice(getPrice.textContent);
                } else {
                    price = getPrice;
                }

                var getName = document.getElementById("a-size-large") ||
                    document.getElementById("productTitle") || "Default Name";
                if (getName.textContent) {
                    product_name = getName.textContent.trim();
                } else {
                    product_name = getName;
                }
            }

            var sendCurrentData = {
                email: userEmail,
                search_history: currenturl,
                price,
                product_name
            };

            sendPOSTDB(sendCurrentData, DB.HistoryEndpoint.url);

            chrome.storage.sync.get(['searched_websites'], function(data) {
                var current = data.searched_websites;
                current++;

                if (current >= 3) {
                    chrome.runtime.sendMessage({
                        action: "addnotification",
                        forumUrl
                    });
                }

                chrome.storage.sync.set({
                    searched_websites: current
                }, function() {
                    if (DB.PostDBisActive === false) {
                        console.log('searched_websites is set to ' + current);
                    }
                });
            });
        }

        // To activate initRun on debug
        if (DB.PostDBisActive === false) {
            chrome.storage.sync.get(['searched_websites'], function(data) {
                var current = data.searched_websites;
                if (current >= 2) {
                    removeLastSync();
                }
            });
        }
    }
});