'use strict';

// Message action from content.js
chrome.runtime.onMessage.addListener((msg, sender, sendResponse) => {
    if (msg.action.localeCompare("getHistory") === 0) {
        chrome.history.search({
            text: msg.text,
            startTime: msg.startTime,
            endTime: msg.endTime,
            maxResults: msg.initmaxHistoryFetch
        }, function(history) {
            sendResponse({
                history
            });
        });
    } else if (msg.action.localeCompare("getUserEmail") === 0) {
        chrome.identity.getProfileUserInfo(function(user) {
            sendResponse({
                user
            });
            return true;
        });
    } else if (msg.action.localeCompare("addnotification") === 0) {
        chrome.notifications.create(msg.forumUrl, {
            type: "basic",
            iconUrl: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAclBMVEX///8AfbgAe7cAcrMAdrUAebYAdLQAcbIAercxir6HtdXI3evn8feMuNdno8v6/f6ry+G0zuPe6/Nyqs8Sgbo2jcChxd7F2+pgoMrX5vEVg7tJlcTw9vqbwdxvqM5Znci61OaJttZCksIAa7B9r9IAZq4UDMWQAAAPvUlEQVR4nN1d2YKqOBBtQhJARUTEFZX2Mv//iwNuqQoJoIStz9PMbQQOSWpLVeXnp3Mk61249aPl6eYdNxtrszl68Wkf/Z7D3Trp/vGdYrE7X2LicEopYy6xiPVA/l8uYYxS26He0k/XQ7/oN0jS3xOzc2bkRUsDkhPlNJ6li6Ff+QMkYbRxaDFojZHztNlyNQmWB9/L2TXnhllust3QBKoxj1zOPhi6Mhin+zQYmocGh4y2pPccS+rs06HJlJGcN0boPUlylo1LwM6vNjXF7smR2XE4NK03VkdubPQgSUr9MVgEwZk2Gr5Cxeeq7wHGSBNdQqh9GXqyBjNOq1811wGcO/R42mcz/7wtcPZ/s+XJo47Nad3aZc5+SI7BzK7QfIV+45u9v5ovNOJ/MQ/9/ZFW8yTOdTCOFfOzYOdFq0MTzRYcVplnV0hi4lwGWY+hpZmfOTvH83efqe1g7seOdiwZ/+2IhR4Hj2vo2ezypVESpHqTiLJ+dUdwcZQvwjjJ5q3uPM8srlzbhMc9LseQqV6ikO3t6D0wj9Ty2XVmBu7eBMlJpeCZ7a2MWcxhrBTSdHMw9YTKpyt8o2L4zD58rR5IJzP6FBWCa3kAuzGvEp8qONJjx6tx7pYGkFCy7ehpW1LmSJxzR0+7wy+LUMq64ldg65aNCn7qzEMOTqXHMe539bQnzmXlwUhHAudQmqG5OdV9wCGIHFfm6Ky6eFLoyPz4rR8dvL6VpJsdmX/MTCbIaCcfUomyhqI307PnKi1BYi/7jIgFS1nGsY3R4GoQSwSZ23dcc0ekYSTUoLxJNtLd7Wv/Ic1gb8uL0dhXXkgeDbH7W4EQoSxwnNTMjdeSYUG9obYXFp60WMxojbXk6toXE3f9EhdppjoGDKo1vifpRtk2xsoxTXGN54VRAfYVDgxbOG2/+AITZMfhg9DJEQt2p1UAJ8GRaXoz9ZqtIDkATgulERwxwaW5t2yFJaZof79yYjQfeAfW7pfIsHyn36ovbIva/Qdl9fCRhCfWdybWDBPs2tX9DJgi+0pAYH9wZARlivQLM+SACA6wb1AHH61F+2PNHyC9SscjZAQytIqcTwXqCYrR0agJDKw0yGfSxoc/ZqeOXrEt0DB89pZzuAjJsas3bI0jXEv8g1BxAF1eQoe3RXVIkEnywVK8su9+1z+QxCebpj8LoRweKGLRFCuoFpuK/ARGLb5Rpb3iAmWi02yHFkoo4nX8gu3hIf+nyS/wHB1/TusCvi9tsIOK5Kg9nuQ5PUK4FJ36rZQIzFG27+EF22MPl1VcdzUUv+RDQ2goBDBbntfJfrhu2wRAesUODgutvhaKGTZOe1uFJZintDrrBow3odOYowUCqMIr5f8ZqE8+BTn6QtO5Bz+FO47YaFPcgJdRoTFg7KmBYhkT1lDYXHVXBUB1sjHGLaoA9bjWHZqBi/h0xMwDcHhczSAGYLXSTlOrOsG5fomB2AxhPb+eCQCDWiNOgSClXeaqdYUtHESVTlyBISS9v54JAHOFqQybjTXtIcSDyMt/ngs5U2e8jhZwnZVdjCv469h2YZoCyspS+CUB6oSPN0BajQTou9LGMNAmkzNnBC7CZmFyxOZYwX46AMk/sjA5ADlTG+kYMWIgTVL0l0wML52SXygjBLIGG6dAzio0yYQAZI0DnYed+MOE5UyBSDMZwb9zE8VZwwEYLsiHEsldpFHkf8QAxinwcYEkLamRqQHKTBHv9cW/2tOepPk0FSoRiBQR6CbugC9nBiCd8r0nnIg4FRv7hmg9gOVmv4IZQE1KhoBB9GYKpoDNy4UCusLpKsTm/+trpy4QM5K8wjXCu+/KJi3q+qjXk1MmbNNXQA0uQ4XvO/MVOK92H+x/P+r6XF0WfCIeMdNG2tNf8fDqp/mlhQgmLlfsGP5jClBKHbIMm83p7fMT6ioZFuIR+i3LGX8/uyZHC5igT8PtF3BWvLJclSemdM4zajCSe/FEW2lPLIQK09uMIiBflx8Cot9P++VEKn+rZXi/RW1TjjWqCqOx4nLDDKF6f8gVEStWmmyVDItK4MqN89BG6S781ANDaLhJ91dWhdYwtAjXbmbligjXFKkrzU0zBLFtXogaKGhUwq6OYUUlTYJrzXQ12KYZAkfibsGAKJty576eoeVaSoo7ilKptXX0phkGgFGh/oAdpxTDDRha7kbx8r+odJc4QNcu0LaBaYYgcHjfhAImgNKwAkYQfUMurWYlY0hqT8AYePc5d6CFb5zhHmsH4W2oLBrAkNzC1RPb6Ci1V5FT5g64MJnewDwuek/AanPjDIFVw5DNpo4jCoZo23GRYT2AUzu3aG6jGfpzvQsCZr0NNOMMgbPkJLAIVn17DcP821xRTQdMQcZ/IRTcWFgA7zpl4wxBOCq3THeCrzqZSMswF8NwpMSm4wLXQCI7BtZjv0bWOEOQckp3aESV0ryCIarKfe/9YzOGoO5OGa41OnXDMEArD6pD5eVVDFHlydMiihAJFwS8fpJYaQEYZ/gDOJ2B3NH8spIhzLa6/14yY5Dbe8CdC97i1DxDYXuzXxDCcNVlNdUMwds9VjU0YwhqXIXlK2hqZZ7h6f0WLPrZi/9RJ6FUM0SG/PaMzRieggv3WL6CvyXGGS7fr+HugXeoCXfXMIQdCXCjOtRdQpKvqPdKo12TjxiKz05OPzfxVHUVZQ1DuCmJ4MBNrB2V3ETxJ9QZxhBDELa4gUWpScGoY+jLNurjYjuFT9Qb4WtU8m+IoRDx+cXCDtdk69Ux3CHh+brXEcxCyQgnUIGE2EU2xBCowCMIlmoyoeoYLhRtPtE269rCSxCSzyTfzBBDkB21ac8wwaPwuBUIh0jdSDhw0coN4LpgqH6tDxgGKhdZhA1xpAb1BFzIzZmMMQSRGqubMcxv9hgqyU4jDERqdoomoaOcpXIPotfd4twmm+NIDWqy5qvGvguG3chS6x6BwyYObj56VX6YLmRpR/rQuod10P9CDSm30THMEOnDWHz07xiCWrCqE1eQkpijJUiMW23ApolN2qUkumlbmEMlkXsZiKAlzMgu7NK2vgVMQkpLGvz1U9SSa4mWYC5+RNi9C98iEv/T1j8sMnRSVZ9opCSwj3wPcoSmGQr/kERgyrb28e87NIuyDGEwWHpwkY98t1GNM0Q+vhCsmgT2SoZnMOFeG8h7SQ+g5ABswz1dSOMMUZymVawN9iESDUKkcAXsWoDX6atBoWmGONbWJl6KqAB1d8DehEWfKzxAsla00DTNEMdL13VBEi3DAMlEAuWU7DSwe8gNu1Fs8w7rm2YIYt788P2+hY/aUBOphEHq/O2S9U+KIsVw51gwpOlCAczQ8hLNNeX7WXaCtvGb7z1lnrT3VGqWKhndhGXQB8Gd1WHamV3GfwFmaJUvcVCxHTAkC+kJMheUKl29fyhpPUUvMCk2jHbbkIJEDFVwZIYKoNY00v4hqGX/eg+YKw0+jYFTblVtmqHwl+6bvgb28XVHa+CV935q6XLDDME+/n3htc7FqGhLqzBwcplUyuwyzFDOxQA++jf5NIR6VXXtS9nRVaUoGma4kscMjOnHOVG58KnJFJQMHGWfZcMM4VbKfd3FWPI0Z0gYp35teiLMWdBMaMMMPZkQ+K0yN5EowSiny7SOXgFh4AAzBjPk6mc88WBIqy5xAcOglK0PvqDKaNp4Ctz2s1XzzO2ngaNNgEuPqme8cU9H8quvAf7LriRYgBOrCdW0xt3A6faAGAFg0fDnnAE1QV3VHiYxd/vK1gfbfa86PGDVdJar/9PbcZTAOXzXdgH90V29RW9IkYP/AFyIf6pmRthooOBimv0iIIBUEUVcwAiYeIElrl0T3iAsku3rBL6uAE22VPwzqID+QzWkNhDfe6BCpj1NYQMT2K0MSNg/VMsN90rgXvzfqcdH8TcwTf9MTwVsgaZ/sC+G5IqC8KA9rW57EDCN0JYMYaBGJixrgMVWKh6B7P9Ej6FyRA/O4D/QJ0rR9xpKob/Q60sxSlNvuSc13VOsNNgKbJo+FOy5p9pkghl4kxxENITKoBAUtVNsAAJUOlG3CF4A43Ti/UtVfQUKAONUGf0eNWB/We1OP+w2PDnDBvYRRpUsCKA75KR7QevbSsDLJtzPW7cKC1zgUE/JT4Q92SvdP2i6TrevfmWkCdquEz0bQd+R/QGYKDPJ8y3UbaAB0Dk6UzyjpOZ4ixwxEEpTPGem3t5co4zKKchTVP6mV/YC6PhRPrHznkijIwI38BcTO7NL5fiWgc6jm9a5a7yhX4sO+JzS2XnNLU10WPV0zj/8QGogeWqx8YZP8RmWtcd1AWyhfHIncg4pqzHXMFqc0tojTjib7yMLLEA/ncR5wJ/KC2kJj7F/Mj7TuamiEEBi2OLj28rA53J/M82iMR8eL5+t7n5le93YiCligl9qtGBDRktRIvhtYHCB069HJG4yXAFQKklqjIPUeGQsSgOrCctusY+0w4n6dByqXyr9s1ul4qWYorZTaY+QmxU0PYxbB6n3isuG9jQOzMUEWy8duahXX+TUC6Qv/uqh0goyRXvIXamLbZ6g4rN5Q4WnFp5UPmTKJZDEjUUG2rQJ5X4N5qbTTm4jZO/7j4YH+9JbGMzYPlDp6zHS957GjsjFa47RhJGFVbr/ss9hDJaOXHD7vammeUSp9QzrMdU2LDWf7kIxy4I6Fzi3fvb617dSS5hujgORFWP+IZ2o+6kaRI4rP7grH+BQWuwW412nFp3tcnmsWRkDEZzKLcio22UO3NYtlwgzq0vb+FwSaRahpCuOWyJrqWKGXrtdGYdj+aMWJevmF37iUwU/95PQ/ZeIFOXrhPLIrFxdR1zBz+K3PmziuaUqn2d2bE4/hrFCvshN0bpEVl6N94G0IxNlYfOLcvhy/as6pKYjrGNVW7ZCeVizdoJunllc3T2BuqmZl2+IFVV3eiCMk0v6nbQL0sjlcm+f9wTNendnZsqlcidJndjfffZCwdyPnVLrovct7esQXveibO/DobS9bHVoQjM4rDLP1g1ecTMeD1X2ub7qOd7HktPj3g/nC90hQfPQ3x/zq/TsCn7HIfPrDlfdXIU8uUO90zL79c/b7Wq7Pfu/2fLkUceu5vbg56UD8iuwXjp1b3l/U5fcT9x7tgpjxG3wI4s5tzHkRy5mKvOqPQh1lkPHn99YeXaTgfyIH7d6axbSCIfM5EDm5tF1DNNTQnp1jJDMZ2e8GtXwCQTplWtsrqbscsl724465TPYZRu7Vgeo4VLOluFIRw9hsVqyD1kWY2dfz6MRnQ2wTrM4N1ZynnUq/W4SeNEHrcNGhHV6XnrMse86PlfxL7LkZQBwm3q5WdfIfB0xkvUuPP9G+9PNO2421mZz9OLTPvo9h7tDD4vuf59ZzTzmKxoIAAAAAElFTkSuQmCC",
            title: "DELL",
            message: "We have similar products available at DELL store. For more personalised recommended products, click on this notification button.",
            requireInteraction: true
        }, function(notificationId) {});
    }
    return true;
});

// onClick listeners
chrome.notifications.onClicked.addListener(function(notificationId) {
    chrome.tabs.create({
        url: notificationId
    });
});