import { Component, OnInit } from '@angular/core';
import {Cart} from './Cart';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  sumPrice = 0.0;

  constructor(public cart: Cart) {
    cart.cartItems.forEach((item) => {
      this.sumPrice += item.price;
    });
  }

  ngOnInit() {
  }

}
