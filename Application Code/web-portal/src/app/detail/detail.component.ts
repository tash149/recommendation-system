import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {Cart} from '../cart/Cart';

const typeDecorator = Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
});


@typeDecorator
export class DetailComponent implements OnInit {

  private apiUrl = 'https://dellhistory.pythonanywhere.com/product/';
  product: any = {};
  loading = true;
  id: any;
  constructor(private route: ActivatedRoute, private http: HttpClient, public currentCart: Cart) {
    this.id = this.route.snapshot.paramMap.get('id');
    this.getProdDetails();
  }

  ngOnInit() {
  }

  getProdDetails() {
    return this.http.get(this.apiUrl + this.id).subscribe(data => {
      this.product = data[0];
      this.loading = false;
      console.log(this.product);
    });
  }

  addToCart() {
    this.currentCart.cartItems.push(this.product);
  }

}
