import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DetailComponent} from './detail/detail.component';
import {HomeComponent} from './home/home.component';
import {CartComponent} from './cart/cart.component';
import {ConfirmationComponent} from './confirmation/confirmation.component';
import {LoginComponent} from "./login/login.component";
import {AnalyticsComponent} from "./analytics/analytics.component";

const appRoutes: Routes = [
  { path: 'detail/:id', component: DetailComponent },
  { path: 'confirmation', component: ConfirmationComponent },
  { path: 'cart', component: CartComponent },
  { path: 'login', component: LoginComponent },
  { path: 'analytics', component: AnalyticsComponent},
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
