import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private apiUrl = 'https://dellhistory.pythonanywhere.com/';
  products: any = {};
  otherProducts: any = {};
  err: string;
  lat: any;
  long: any;

  constructor(private http: HttpClient) {
    this.getProducts();
    navigator.geolocation.getCurrentPosition(resp => {
      this.lat = resp.coords.latitude;
      this.long = resp.coords.longitude;
      console.log(this.lat);
      console.log(this.long);
    },
      err => {
        this.err = err.toString();
      });
  }

  getProducts() {
    return this.http.get(this.apiUrl + 'history/last/').subscribe(data => {
      const tag = data[0];
      const price = data[1];
      const passData = {
        'tag': tag,
        'price': price
      };
      this.http.post(this.apiUrl + 'rec_product/', passData).subscribe(
        products => {
          this.products = products;
          if (this.products.length === 0) {
            console.log('balblabla');
            this.http.get(this.apiUrl + 'rand_products/').subscribe(
              newProducts => {
                this.otherProducts = newProducts;
              }
            );
          }
          console.log(this.products);
          console.log(this.otherProducts);
        }
      );
    });
  }

  ngOnInit() {
  }

}
